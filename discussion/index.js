//QUERY OPERATORS

/*
	Comparison Query Operators

	$gt --> greater than operator
		Syntax:
			db.collectionName.find({field: {$gt: value}})

	$gte --> greater than or equal to operator
		Syntax:
			db.collectionName.find({field: {$gte: value}})

	$lt --> less than operator
		Syntax:
			db.collectionName.find({field: {$lt: value}})	

	$lte --> less than or equal to operator
		Syntax:
			db.collectionName.find({field: {$lte: value}})	

	$ne --> not equal operator
		Syntax:
			db.collectionName.find({field: {$ne: value}})

	$in --> in operator finds documents with specific match criteria.
		Syntax:		
			db.collectionName.find( {field: {$ne: value}} )

*/
	// EXAMPLES:
	//$gt
	db.users.find({age: {$gt: 76}});
	//$lt
	db.users.find({age: {$lt: 76}});
	//$ne
	db.users.find({age: {$ne: 76}});
	//$in
	db.users.find({courses: {$in: ["Sass", "React"]}});


/*
	Logical Query Operators

	$or
		Syntax:
			db.collectionName.find({$or: [{fieldA: valueA}, {fieldB: valueB}]})

	$and
		Syntax:
			db.collectionName.find({$and: [{fieldA, valueA}, {fieldB: valueB}]})


*/
	//EXAMPLES:
	//$or
	db.users.find({$or: [{firstName: "Neil"}, {age: 25}]});
	db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]});

	//$and
	db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});


/*
	Evaluation Query Operator

	$regex --> used to look for a partial match in a given field. It is used to search for STRINGS in a collection. It i
		Syntax:
			db.collectionName.find({field: {$regex: 'value/keyword', $options: '$i'}})

	$options with "$i" parameter --> specifics that we want to carry out search without considering the upper and lower cases.

	To know more: https://docs.mongodb.com/manual/reference/operator/query/regex/
*/

	//EXAMPLES:
	//case sensitive
	db.users.find({firstName: {$regex: "N"}});
	//case insensitive because of $options: "$i"
	db.users.find({firstName: {$regex: "j", $options: "$i"}});


// FIELD PROJECTION

/*
	If you do not want to retrieve all the field/properties of a document, you can use field projection.

	Inclusion
		--> includes/adds specific field only when retrieving documents
		--> value of 1 --> to denote that the field is being included.
		Syntax:
			db.collectionName.find({criteria}, {field: 1})

	Exclusion
		-->excludes/removes specific field only when retrieving documents
		--> value of 0 --> to denote that the field is being excluded.
		Syntax:
			db.collectionName.find({criteria}, {field: 0})

*/

	//EXAMPLES:
	//Includes the firstName and _id
	db.users.find({firstName: "Jane"}, {firstName: 1});

	//Excludes the _id but includes the firstName
	db.users.find({firstName: "Jane"}, {
		firstName: 1, 
		_id: 0
	});

	//Excludes the _id but includes the contact -> phone (nested field)
	db.users.find({firstName: "Jane"}, {
		"contact.phone": 1,
		_id: 0
    });


/*
	Slice Operator
	$slice --> retrieves portions of element that matches the search criteria.
		Syntax: 
			db.collectionName.find({criteria}, {field: {$slice: count| [skip, limit]}})


	Parameters: count | [skip, limit]
		count --> number of items to be retrieved from the array. If a number of counts is more than the number of items in the array, then all the items will be retrieved from the array.
		skip --> a number of items to skip from the array.
		limit --> number or items to be retrieved from the array.
*/

	//EXAMPLE
	db.users.find({}, {
		_id: 0,
		courses: {$slice: [1, 2]}
    });

/*
ACTIVITY:
	1. Create an activity.js file on where to write and save the solution for the activity.
	2. Find users with letter s in their first name or d in their last name.
	- Use the $or operator.
	- Show only the firstName and lastName fields and hide the _id field.
	3. Find users who are from the HR department and their age is greater then or equal to 70.
	- Use the $and operator
	4. Find users with the letter e in their first name and has an age of less than or equal to 30.
	- Use the $and, $regex and $lte operators
	5. Create a git repository named S24.
	6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code. "s24 activity"
	7. Add the link in Boodle. "MongoDB - Query Operators and Field Projection"   
*/ 